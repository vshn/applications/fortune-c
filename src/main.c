#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <microhttpd.h>
#include <json-c/json.h>
#include <unistd.h>

#define PORT 8080
#define BUFSIZE 128
#define VERSION "1.2-c"

// Handling of CTRL+C courtesy of
// https://stackoverflow.com/a/17766999/133764
volatile sig_atomic_t flag = 0;
void signal_handler(int sig) {
    flag = 1;
}

int execute_command(char *output, const char *cmd) {
    FILE *pipe;
    char buffer[BUFSIZE] = {0};
    if ((pipe = popen(cmd, "r")) == NULL) {
        return 1;
    }
    while (fgets(buffer, BUFSIZE, pipe) != NULL) {
        strcat(output, buffer);
    }
    pclose(pipe);
    return 0;
}

int respond_html(char *output, const char *fortune, const char *hostname, int random)
{
    char *template =
    #include "../templates/fortune.tpl"
    ;

    sprintf(output, template, random, fortune, VERSION, hostname);
    return 0;
}

int respond_text(char *output, const char *fortune, int random)
{
    char *template = "Fortune %s cookie of the day #%d:\n\n%s";
    sprintf(output, template, VERSION, random, fortune);
    return 0;
}

int respond_json(char *output, const char *fortune, const char *hostname, int random)
{
    json_object *root = json_object_new_object();
    if (!root) {
        return 1;
    }
    json_object_object_add(root, "version", json_object_new_string(VERSION));
    json_object_object_add(root, "number", json_object_new_int(random));
    json_object_object_add(root, "message", json_object_new_string(fortune));
    json_object_object_add(root, "hostname", json_object_new_string(hostname));

    strcat(output, json_object_to_json_string_ext(root, JSON_C_TO_STRING_PRETTY));
    json_object_put(root);
    return 0;
}

// tag::router[]
int handler(void *cls, struct MHD_Connection *connection,
                         const char *url,
                         const char *method, const char *version,
                         const char *upload_data,
                         size_t *upload_data_size, void **con_cls)
{
    char fortune[100000] = { "" };
    int result = execute_command(fortune, "fortune");
    char hostname[100] = { "" };
    result = execute_command(hostname, "hostname");
    int random = rand() % 1000;

    char page[200000] = { "" };
    const char *accept_header = MHD_lookup_connection_value (connection, MHD_HEADER_KIND, "Accept");
    if (strcmp(accept_header, "application/json") == 0) {
        result = respond_json(page, fortune, hostname, random);
    }
    else if (strcmp(accept_header, "text/plain") == 0) {
        result = respond_text(page, fortune, random);
    }
    else {
        result = respond_html(page, fortune, hostname, random);
    }
    struct MHD_Response *response;
    int ret;

    response = MHD_create_response_from_buffer(strlen(page),
                                               (void *)page, MHD_RESPMEM_PERSISTENT);
    ret = MHD_queue_response(connection, MHD_HTTP_OK, response);
    MHD_destroy_response(response);

    return ret;
}
// end::router[]

int main(void)
{
    signal(SIGINT, signal_handler);
    struct MHD_Daemon *daemon;

    daemon = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION, PORT, NULL, NULL,
                              &handler, NULL, MHD_OPTION_END);
    while (1) {
        sleep(1);
        if (flag == 1) {
            break;
        }
    }

    MHD_stop_daemon(daemon);
    return EXIT_SUCCESS;
}
