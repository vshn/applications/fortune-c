# Step 1: Builder image
FROM debian:stable-slim AS builder

# Install build requirements
RUN apt-get update && apt-get install -y build-essential cmake zip unzip curl git tar pkg-config bison libmicrohttpd-dev fortune

# Install JSON-C
RUN git clone https://github.com/json-c/json-c.git /json-c
WORKDIR /json-c-build
RUN cmake ../json-c
RUN make && make install

# Build application
WORKDIR /builder
COPY CMakeLists.txt .
COPY src /builder/src
COPY templates /builder/templates
RUN cmake -B build -S .
RUN cmake --build build

# tag::production[]
# Step 2: Production image
FROM busybox:glibc
COPY --from=builder /builder/build/c-fortune /usr/local/bin/c-fortune
COPY --from=builder /usr/local/lib/libjson-c.so.5 /usr/local/lib/libjson-c.so.5
COPY --from=builder /usr/games/fortune /usr/local/bin/fortune
COPY --from=builder /usr/share/games/fortunes /usr/share/games/fortunes
COPY --from=builder /usr/lib/x86_64-linux-gnu/libmicrohttpd.so.12 /usr/lib/x86_64-linux-gnu/libmicrohttpd.so.12
COPY --from=builder /usr/lib/x86_64-linux-gnu/libgnutls.so.30 /usr/lib/x86_64-linux-gnu/libgnutls.so.30
COPY --from=builder /usr/lib/x86_64-linux-gnu/libp11-kit.so.0 /usr/lib/x86_64-linux-gnu/libp11-kit.so.0
COPY --from=builder /usr/lib/x86_64-linux-gnu/libidn2.so.0 /usr/lib/x86_64-linux-gnu/libidn2.so.0
COPY --from=builder /usr/lib/x86_64-linux-gnu/libunistring.so.2 /usr/lib/x86_64-linux-gnu/libunistring.so.2
COPY --from=builder /usr/lib/x86_64-linux-gnu/libtasn1.so.6 /usr/lib/x86_64-linux-gnu/libtasn1.so.6
COPY --from=builder /usr/lib/x86_64-linux-gnu/libnettle.so.8 /usr/lib/x86_64-linux-gnu/libnettle.so.8
COPY --from=builder /usr/lib/x86_64-linux-gnu/libhogweed.so.6 /usr/lib/x86_64-linux-gnu/libhogweed.so.6
COPY --from=builder /usr/lib/x86_64-linux-gnu/libgmp.so.10 /usr/lib/x86_64-linux-gnu/libgmp.so.10
COPY --from=builder /usr/lib/x86_64-linux-gnu/libffi.so.7 /usr/lib/x86_64-linux-gnu/libffi.so.7
COPY --from=builder /usr/lib/x86_64-linux-gnu/librecode.so.0 /usr/lib/x86_64-linux-gnu/librecode.so.0
COPY --from=builder /lib/x86_64-linux-gnu/libdl.so.2 /lib/x86_64-linux-gnu/libdl.so.2

EXPOSE 8080

# <1>
USER 1001:0

CMD ["/usr/local/bin/c-fortune"]
# end::production[]
